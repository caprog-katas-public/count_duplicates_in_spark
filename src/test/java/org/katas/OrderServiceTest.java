package org.katas;

import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.junit.jupiter.api.Test;
import org.katas.entity.Order;

import java.util.List;

import static org.apache.spark.sql.functions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class OrderServiceTest {

    private static final String PENDING = "PENDING";
    private static final String DATE_2022_08_29 = "2022-08-29";
    private static final String DATE_2022_08_28 = "2022-08-28";
    private static final String SHIPPED = "SHIPPED";
    public static final String CANCELED = "CANCELED";

    @Test
    void should_count_orders_by_status_and_date_given_orders() {
        //Given
        List<Order> orders = List.of(
                new Order("005", PENDING, DATE_2022_08_29),
                new Order("006", PENDING, DATE_2022_08_29),
                new Order("001", SHIPPED, DATE_2022_08_28),
                new Order("002", SHIPPED, DATE_2022_08_28),
                new Order("003", "CANCELED", DATE_2022_08_28)
        );
        SparkSession session = getSparkSession();
        Dataset<Row> ordersDataset = createDataset(session, orders);

        //When
        Dataset<Row> ordersCountedByStatusAndDate = countByStatusAndDate(ordersDataset);

        //Then
        boolean hasTwoStatusPending = hasStatusAndCount(ordersCountedByStatusAndDate, PENDING, 2);
        boolean hasTwoStatusShipped = hasStatusAndCount(ordersCountedByStatusAndDate, SHIPPED, 2);
        boolean hasOneStatusCanceled = hasStatusAndCount(ordersCountedByStatusAndDate, CANCELED, 1);

        assertTrue(hasTwoStatusPending);
        assertTrue(hasTwoStatusShipped);
        assertTrue(hasOneStatusCanceled);
    }

    private boolean hasStatusAndCount(Dataset<Row> ordersCountedByStatusAndDate, String status, int count) {
        return ordersCountedByStatusAndDate
                .filter(hasStatus(status).and(hasCount(count)))
                .count() == 1;
    }

    private Column hasCount(int value) {
        return col("count").equalTo(value);
    }

    private Column hasStatus(String status) {
        return col("status").equalTo(status);
    }

    private Dataset<Row> countByStatusAndDate(Dataset<Row> ordersDataset) {
        return ordersDataset
                .groupBy("status", "date")
                .agg(count(lit(1)).as("count"));
    }

    private Dataset<Row> createDataset(SparkSession session, List<Order> orders) {
        return session.createDataFrame(orders, orders.get(0).getClass());
    }

    private SparkSession getSparkSession() {
        return SparkSession.builder()
                .appName("Count Duplicates")
                .config("spark.master", "local")
                .getOrCreate();
    }
}
