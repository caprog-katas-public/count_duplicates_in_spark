# Count Duplicates Data In Spark

## Technologies

- IntelliJ Community 2021.1.1
- Maven 3.6.3
- Java 11
- Apache Spark 3.3.0
- JUnit 5.9.0
- Project Lombok 1.18.24

## Description

This is a project for the **Kata** ``count orders by status and date`` presented in my personal blog page.

## Run tests

Go to ``org.katas.OrderServiceTest`` and run the test method ``should_count_orders_by_status_and_date_given_orders``. Otherwise, run test with maven command ``mvn test``.
